import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  menuState: string = 'out';
  parsedRecords: any[] = [];
  csvContent: string;
  isXmlFile: boolean = true;
  file: any;

  constructor() {
  }



  ngOnInit() {
  }

  /**
   * Parsing xml file content.
   *
   * @param {*} xmlString
   * @memberof AppComponent
   */
  private parseXmlFile(xmlString: any): void {

    const parser = new DOMParser();

    const dom = parser.parseFromString(xmlString, 'text/xml');

    const records = Array.from(dom.querySelectorAll('records record'));

    this.parsedRecords = records.map(recordEl => {
      return this.parseRecordEl(recordEl);
    });

  }

  /**
   * Forming array from parsed records.
   *
   * @param {*} el
   * @returns
   * @memberof AppComponent
   */
  private parseRecordEl(el) {
    const record = {};
    record['reference'] = el.getAttribute('reference');
    Array.from(el.children).forEach(childEl => {
      record[childEl['nodeName']] = childEl['textContent'];
    });

    return record;
  }

  /**
   * This will be called while selecting file and it is used for extracting selected file.
   *
   * @param {*} e
   * @memberof AppComponent
   */
  fileChanged(e) {
    this.file = e.target.files[0];
    this.parsedRecords = [];
    this.isXmlFile = this.validateFileTpe();

  }

  /**
   * Validating file type.
   *
   * @returns {boolean}
   * @memberof AppComponent
   */
  private validateFileTpe(): boolean {
    if ('text/xml' === this.file.type) {
      return true;
    } else if ('text/csv' === this.file.type) {
      return false;
    } else {
      // Notify please upload csvor xml file
      this.file = null;
      alert('Please select xml or csv file to parse and validate records.');
      return;
    }
  }

  /**
   * Read file content.
   *
   * @returns
   * @memberof AppComponent
   */
  uploadDocument(): void {
    const fileReader = new FileReader();
    if (!!!this.file) {
      return;
    }
    fileReader.onload = e => {

      if (!!this.isXmlFile) {
        this.parseXmlFile(fileReader.result);
      } else {
        this.parsedRecords = this.csvJSON(fileReader.result);
      }

      this.validateRecords();

    };
    fileReader.readAsText(this.file);
  }

  /**
   * Validating records.
   *
   * @private
   * @memberof AppComponent
   */
  private validateRecords(): void {

    for (let incr = 0; incr < this.parsedRecords.length; incr++ ) {
      const recValue = this.parsedRecords[incr];
      for (let jncr = incr + 1; jncr < this.parsedRecords.length; jncr++ ) {
        const tempRecValue = this.parsedRecords[jncr];
        if ((+tempRecValue.reference === +recValue.reference && this.isXmlFile) ||
        (+tempRecValue.Reference === +recValue.Reference && !this.isXmlFile)) {
          this.parsedRecords[+incr]['failed'] = true;
          this.parsedRecords[+jncr]['failed'] = true;
        }
      }

      // validating end balance..
      this.validateEndBalance(recValue);

    }
  }

  /**
   * Used to form json object from csv file content.
   *
   * @private
   * @param {*} [csv]
   * @returns
   * @memberof AppComponent
   */
  private csvJSON(csv?: any) {
    const lines = csv.split('\n');

    const result = [];

    const headers = lines[0].split(',');

    for (let incr = 1; incr < lines.length - 1; incr++) {

      const obj = {};
      const currentline = lines[incr].split(',');

      for (let jincr = 0; jincr < headers.length; jincr++) {

        obj[headers[jincr].replace(' ', '_')] = currentline[jincr];
      }

      result.push(obj);
    }

    // JSON
    return JSON.parse(JSON.stringify(result));
  }

  /**
   * Validating end balance.
   *
   * @private
   * @param {*} [record]
   * @memberof AppComponent
   */
  private validateEndBalance(record?: any): void {
    if ((+record.endBalance !== (+record.startBalance + (+record.mutation))  && this.isXmlFile) ||
    ((+record.End_Balance !== (+record.Start_Balance + (+record.Mutation)) && !this.isXmlFile))) {
      record['failed'] = true;
    } else {
      record['failed'] = false;
    }
  }

}
